{
  description = "Visualize `strace(1)`-output";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.05;
    flake-utils.url = github:numtide/flake-utils;
    build-rust-flake.url = gitlab:ma27/build-rust-flake;
  };

  outputs = { self, nixpkgs, build-rust-flake, flake-utils }: {
    overlay = final: prev: let
      buildRustCode = build-rust-flake.builders.buildRustPackage { pkgs = final; };
    in {
      strace-visualize = buildRustCode {
        pname = "strace-visualize";
        version = "0.0.1-dev";
        src = final.lib.cleanSource ./.;
      };
    };

    hydraJobs.package.x86_64-linux = self.defaultPackage.x86_64-linux;
  } // flake-utils.lib.eachSystem [ "x86_64-linux" "aarch64-linux" ] (system: let
    pkgs = import nixpkgs {
      inherit system;
      overlays = [ self.overlay ];
    };
  in {
    packages = { inherit (pkgs) strace-visualize; };
    defaultPackage = pkgs.strace-visualize;

    devShell = with pkgs; mkShell {
      name = "devshell";
      buildInputs = [
        cargo rustc rustfmt rustPackages.clippy
      ];
    };
  });
}
