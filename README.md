# strace-visualize

[![pipeline status](https://img.shields.io/endpoint?url=https://hydra.ist.nicht-so.sexy/job/strace-visualize/master/package.x86_64-linux/shield)](https://hydra.ist.nicht-so.sexy/job/strace-visualize/master/package.x86_64-linux/)

Visualize `strace(1)`-output.
