//use std::io::{self, Read};

mod parser;
use std::fs;

fn main() {
    println!(
        "{:?}",
        parser::strace_line("foo(a,b,c,d)    = -1 EINVAL (foo)")
    );

    println!(
        "{:?}",
        parser::strace_line("mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f256d477000")
    );

    //println!(
    //"{}",
    //parser::strace_line("mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f256d477000").unwrap().1.ret.unwrap().repr.code()
    //);

    let line = "openat(AT_FDCWD, \"/nix/store/s7l9iwyzkny13sc88appsjf4xrzc5kaa-attr-2.4.48/lib/libattr.so.1\", O_RDONLY|O_CLOEXEC) = 3";

    println!("{:?}", parser::strace_line(line));

    println!(
        "{:?}",
        parser::strace_line(
            "access(\"/etc/ld-nix.so.preload\", R_OK)  = -1 ENOENT (No such file or directory)"
        )
    );

    println!(
        "{:?}",
        parser::strace_line("execve(\"/nix/store/vr96j3cxj75xsczl8pzrgsv1k57hcxyp-coreutils-8.31/bin/echo\", [\"echo\", \"hello\", 0xfff], 0x7ffd09581bb8 /* 135 vars */) = 0")
    );

    println!(
        "{:?}",
        parser::strace_line("mmap(0x7f67eeb58000, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED|MAP_DENYWRITE, 3, 0x2000) = 0x7f67eeb58000"),
    );

    println!(
        "{:?}",
        parser::strace_line("fstat(3, {st_mode=S_IFREG|0555, st_size=45104, ...}) = 0")
    );

    let line = "fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x3), ...}) = 0";
    println!("{:?}", parser::strace_line(line));

    println!(
        "{:?}",
        parser::strace_line(
            "prlimit64(0, RLIMIT_STACK, NULL, {rlim_cur=8192*1024, rlim_max=RLIM64_INFINITY}) = 0"
        )
    );

    println!(
        "{:?}",
        parser::strace_line("exit_group(0)                           = ?")
    );

    let contents = fs::read_to_string("/home/ma27/Projects/strace-visualize/tmp");
    println!("{:?}", parser::parse(&contents.unwrap()));

    println!(
        "{:?}",
        parser::strace_line(
            "16:16:43 fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x1), ...}) = 0"
        )
    );

    let lines = "16:30:31 exit_group(0)                  = ?\n16:30:31 +++ exited with 0 +++";
    let result = parser::parse(lines);
    println!("{:?}", result);

    println!(
        "{:?}",
        parser::strace_line(
            "fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x1), ...}) = 0 <0.000019>"
        )
    );
}

//fn main() -> io::Result<()> {
//let mut buf = String::new();
//io::stdin().read_to_string(&mut buf)?;
//println!("{}", buf);
//Ok(())
//}
