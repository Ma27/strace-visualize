use nom::{
    branch::alt,
    bytes::complete::{is_not, tag, take_till, take_until, take_while, take_while_m_n},
    character::complete,
    combinator::{all_consuming, map_parser, map_res, opt},
    error::{context, Error, ErrorKind},
    sequence::{delimited, preceded, terminated, tuple},
    IResult,
};

use std::collections::HashMap;

use itertools::Itertools;

#[derive(Debug, PartialEq, Clone)]
pub struct BitFlags {
    flags: Vec<String>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct LibcCall {
    name: String,
    args: Vec<SyscallArg>,
}

#[derive(Debug, PartialEq, Clone)]
pub enum SyscallArg {
    Literal(String),
    NULL,
    Flag(BitFlags),
    Omitted(u16),
    Pointer(NumRepr),
    Array(Vec<SyscallArg>),
    Struct(HashMap<String, SyscallArg>),
    LibcCall(LibcCall),
    ArbitraryNum(NumRepr),
}

#[derive(Debug, Clone)]
pub enum ParsedLine {
    Full(Line),
    Opaque(Line),
    Resume((String, Option<Vec<SyscallArg>>, Option<Return>)),
}

#[derive(Debug, PartialEq, Clone)]
pub struct NumRepr {
    num: i64,
    base: u8,
}

#[derive(Debug, Clone)]
pub struct Return {
    pub repr: NumRepr,
    r#const: Option<String>,
    phrase: Option<String>,
}

impl NumRepr {
    pub fn code(&self) -> String {
        if self.base == 0 {
            format!("unknown")
        } else if self.base == 8 {
            format!("{:#o}", self.num)
        } else if self.base == 16 {
            format!("{:#X}", self.num)
        } else {
            format!("{}", self.num)
        }
    }
}

#[derive(Debug, Clone)]
pub struct Time {
    hour: u8,
    minute: u8,
    second: u8,
}

#[derive(Debug, Clone)]
pub struct Line {
    pub from_pid: Option<u64>,
    pub time: Option<Time>,
    pub syscall: String,
    pub argstr: Vec<SyscallArg>,
    pub ret: Option<Return>,
    pub duration: Option<f64>,
    pub is_opaque: bool,
}

impl Line {
    fn with_exit(&self, r: Return, args: Option<Vec<SyscallArg>>) -> Line {
        let mut all = self.argstr.clone();
        if let Some(mut a) = args {
            all.append(&mut a);
        }
        Line {
            from_pid: self.from_pid,
            time: self.time.clone(),
            syscall: self.syscall.clone(),
            argstr: all,
            duration: self.duration,
            is_opaque: false,
            ret: Some(r),
        }
    }
}

#[derive(Debug, Clone)]
pub struct OpaqueOutput {
    pub started: Vec<Line>,
    pub resumed: Vec<(String, Option<Vec<SyscallArg>>, Option<Return>)>,
}

impl OpaqueOutput {
    pub fn join(&mut self) -> Option<Vec<Line>> {
        let mut lines = vec![];
        for line in &self.started {
            if !line.is_opaque {
                lines.push(line.clone());
            } else {
                if self.resumed.len() == 0 {
                    return None;
                }
                let (syscall, a, r) = self.resumed.pop().unwrap();
                if syscall != line.syscall {
                    return None;
                }

                // FIXME start/end time
                lines.push(line.with_exit(r.unwrap(), a).clone());
            }
        }

        Some(lines.to_owned())
    }
}

#[derive(Debug, Clone)]
pub struct Process {
    pub syscalls: Vec<Line>,
    pub exit: Option<u8>,
    pub pid: Option<u64>,
    pub children: Vec<Process>,
}

fn spacing(i: &str) -> IResult<&str, &str> {
    let chars = " \t\r\n";
    take_while(move |c| chars.contains(c))(i)
}

fn omitted_args_at_the_end(close: &'static str) -> impl Fn(&str) -> IResult<&str, &str> {
    move |i: &str| {
        if close == "]" {
            alt((
                take_while(|x: char| x != ',' && x != ']'),
                take_until(" /* "),
            ))(i)
        } else {
            alt((
                take_until(","),
                take_until(" /* "),
                take_until(close),
                take_till(|x| x == close.chars().last().unwrap()),
            ))(i)
        }
    }
}

fn full_argstr(argstr: Vec<SyscallArg>, omitted: Option<SyscallArg>) -> Vec<SyscallArg> {
    match omitted {
        None => argstr,
        Some(n) => {
            let mut cp = argstr.clone();
            cp.append(&mut vec![n]);
            cp
        }
    }
}

fn parse_bitflag(close: &'static str) -> impl Fn(&str) -> IResult<&str, SyscallArg> {
    move |x: &str| {
        map_res(
            tuple((
                is_not("|,"),
                tag("|"),
                nom::multi::separated_list0(tag("|"), omitted_args_at_the_end(close)),
            )),
            |(a, _, b)| -> Result<SyscallArg, &str> {
                let mut flags = vec![String::from(a)];
                let mut tail = b.iter().map(|y| String::from(*y)).collect::<Vec<_>>();
                flags.append(&mut tail);
                Ok(SyscallArg::Flag(BitFlags { flags }))
            },
        )(x)
    }
}

fn parse_libc_call(n: &str) -> IResult<&str, SyscallArg> {
    map_res(
        tuple((
            is_not("(,"),
            context(
                "libc call",
                preceded(
                    tag("("),
                    terminated(map_parser(is_not(")"), parse_arglist(")")), tag(")")),
                ),
            ),
        )),
        |(name, args): (&str, Vec<SyscallArg>)| -> Result<SyscallArg, &str> {
            Ok(SyscallArg::LibcCall(LibcCall {
                name: String::from(name),
                args,
            }))
        },
    )(n)
}

fn read_struct(n: &str) -> IResult<&str, HashMap<String, SyscallArg>> {
    map_res(
        nom::multi::separated_list0(
            tag(","),
            alt((
                context(
                    "struct value",
                    tuple((
                        alt((is_not("="), is_not(","))),
                        tag("="),
                        alt((
                            parse_libc_call,
                            parse_bitflag("}"),
                            map_parser(take_until(","), parse_literal(")")),
                            map_parser(take_until("}"), parse_literal(")")),
                        )),
                    )),
                ),
                tuple((
                    tag(" "),
                    tag("..."),
                    map_res(tag(""), |_| -> Result<SyscallArg, &str> {
                        Ok(SyscallArg::Omitted(0))
                    }),
                )),
            )),
        ),
        |x| -> Result<HashMap<String, SyscallArg>, &str> {
            Ok(x.iter()
                .map(|(a, b, c)| {
                    let a = match *b {
                        "..." => String::from("000-omitted"),
                        _ => String::from(*a).trim_start().to_string(),
                    };
                    (a, c.clone())
                })
                .collect::<HashMap<_, _>>())
        },
    )(n)
}

fn parse_literal(close: &'static str) -> impl Fn(&str) -> IResult<&str, SyscallArg> {
    move |s: &str| {
        preceded(
            spacing,
            map_parser(
                omitted_args_at_the_end(close),
                alt((
                    map_res(tag("NULL"), |_| -> Result<SyscallArg, &str> {
                        Ok(SyscallArg::NULL)
                    }),
                    map_res(
                        tuple((tag("0x"), complete::hex_digit1)),
                        |(_, n)| -> Result<SyscallArg, &str> {
                            Ok(SyscallArg::Pointer(NumRepr {
                                base: 16,
                                num: i64::from_str_radix(n, 16).unwrap(),
                            }))
                        },
                    ),
                    map_res(
                        tuple((tag("0"), complete::digit1)),
                        |(_, n)| -> Result<SyscallArg, &str> {
                            Ok(SyscallArg::ArbitraryNum(NumRepr {
                                base: 8,
                                num: i64::from_str_radix(n, 8).unwrap(),
                            }))
                        },
                    ),
                    map_res(take_while(|_| true), |x| -> Result<SyscallArg, &str> {
                        Ok(SyscallArg::Literal(String::from(x)))
                    }),
                )),
            ),
        )(s)
    }
}

fn parse_arglist(close: &'static str) -> impl Fn(&str) -> IResult<&str, Vec<SyscallArg>> {
    move |i: &str| {
        nom::multi::separated_list0(
            tag(","),
            alt((
                map_res(
                    preceded(
                        preceded(spacing, tag("{")),
                        terminated(read_struct, tag("}")),
                    ),
                    |a| -> Result<SyscallArg, &str> { Ok(SyscallArg::Struct(a)) },
                ),
                parse_bitflag(close),
                map_res(
                    tuple((preceded(spacing, tag("[")), parse_arglist("]"), tag("]"))),
                    |(_, args, _)| -> Result<SyscallArg, &str> { Ok(SyscallArg::Array(args)) },
                ),
                parse_literal(close),
            )),
        )(i)
    }
}

fn match_time(i: &str) -> IResult<&str, (&str, &str, &str, &str, &str, &str)> {
    tuple((
        take_while_m_n(2, 2, |x: char| x.is_digit(10)),
        tag(":"),
        take_while_m_n(2, 2, |x: char| x.is_digit(10)),
        tag(":"),
        take_while_m_n(2, 2, |x: char| x.is_digit(10)),
        tag(" "),
    ))(i)
}

fn exit_code(i: &str) -> IResult<&str, (Option<Return>, Option<f64>)> {
    map_res(
        tuple((
            complete::space0,
            opt(tuple((
                complete::char('='),
                complete::space1,
                map_res(
                    // FIXME incorporate logic from parse_literal here
                    alt((
                        tuple((opt(tag("")), tag(""), tag("?"))),
                        tuple((opt(tag("")), tag("0x"), complete::hex_digit1)),
                        tuple((opt(tag("-")), tag(""), take_while(|x: char| x.is_digit(10)))),
                    )),
                    |(x, pre, input)| {
                        if input == "?" {
                            Ok((0, 0))
                        } else {
                            let rad = if pre == "0x" { 16 } else { 10 };
                            let res = i64::from_str_radix(input, rad);
                            let i = match x {
                                Some(n) => match n {
                                    "-" => res.map(|x| x * (-1)),
                                    _ => res,
                                },
                                None => res,
                            };

                            i.map(|x| (x, if pre == "0x" { 16 } else { 10 }))
                        }
                    },
                ),
                opt(map_res(
                    tuple((
                        complete::space0,
                        is_not(" "),
                        complete::space1,
                        complete::char('('),
                        is_not(")"),
                        complete::char(')'),
                    )),
                    |(_, code, _, _, phrase, _)| -> std::result::Result<(String, String), &str> {
                        Ok((String::from(code), String::from(phrase)))
                    },
                )),
            ))),
            opt(map_res(
                tuple((tag(" <"), is_not(">"), tag(">"))),
                |(_, n, _)| -> Result<f64, std::num::ParseFloatError> {
                    String::from(n).parse::<f64>()
                },
            )),
        )),
        |(_, x, duration)| -> Result<(Option<Return>, Option<f64>), nom::Err<nom::error::Error<&&str>>> {
            let a = match x {
                Some((_, _, (ret, base), details)) => {
                    if base == 16 && details.is_some() {
                        // FIXME nicer feedback here
                        Err(nom::Err::Failure(Error {
                            input: &i,
                            code: ErrorKind::Switch,
                        }))
                    } else {
                        let (r#const, phrase) = match details {
                            Some((x, y)) => (Some(x), Some(y)),
                            None => (None, None),
                        };
                        Ok(Some(Return {
                            repr: NumRepr { num: ret, base },
                            r#const,
                            phrase,
                        }))
                    }
                }
                None => Ok(None),
            };

            a.map(|x| (x, duration))
        }
    )(i)
}

fn line_start(i: &str) -> IResult<&str, (Option<u64>, Option<Time>)> {
    tuple((
        opt(map_res(
            tuple((
                tag("[pid"),
                preceded(spacing, take_while(|x: char| x.is_digit(10))),
                tag("] "),
            )),
            |(_, v, _)| -> Result<u64, &str> { Ok(u64::from_str_radix(v, 10).unwrap()) },
        )),
        opt(map_res(
            match_time,
            |(h, _, m, _, s, _)| -> Result<Time, &str> {
                Ok(Time {
                    hour: u8::from_str_radix(h, 10).unwrap(),
                    minute: u8::from_str_radix(m, 10).unwrap(),
                    second: u8::from_str_radix(s, 10).unwrap(),
                })
            },
        )),
    ))(i)
}

fn syscall(i: &str) -> IResult<&str, &str> {
    is_not(" (")(i)
}

fn args(i: &str) -> IResult<&str, (Vec<SyscallArg>, Option<SyscallArg>)> {
    delimited(
        tag("("),
        tuple((
            parse_arglist(")"),
            opt(map_res(
                tuple((tag(" /* "), complete::digit1, tag(" vars */"))),
                |(_, x, _)| -> Result<SyscallArg, &str> {
                    Ok(SyscallArg::Omitted(u16::from_str_radix(x, 10).unwrap()))
                },
            )),
        )),
        tag(")"),
    )(i)
}

fn args_unfin(i: &str) -> IResult<&str, (Vec<SyscallArg>, Option<SyscallArg>)> {
    delimited(
        tag("("),
        tuple((
            parse_arglist("<unfinished ...>"),
            opt(map_res(
                tuple((tag(" /* "), complete::digit1, tag(" vars */"))),
                |(_, x, _)| -> Result<SyscallArg, &str> {
                    Ok(SyscallArg::Omitted(u16::from_str_radix(x, 10).unwrap()))
                },
            )),
        )),
        tag("<unfinished ...>"),
    )(i)
}

pub fn strace_line(i: &str) -> IResult<&str, ParsedLine> {
    map_parser(
        is_not("\n"),
        all_consuming(alt((
            map_res(
                tuple((
                    line_start,
                    preceded(spacing, tag("<... ")),
                    is_not(" "),
                    tag(" resumed>"),
                    opt(parse_arglist(")")),
                    tag(")"),
                    complete::space0,
                    exit_code,
                )),
                |(_, _, syscall, _, resume_args, _, _, (ret, _))| -> Result<ParsedLine, &str> {
                    Ok(ParsedLine::Resume((String::from(syscall), resume_args, ret)))
                }
            ),
            map_res(
                tuple((
                    line_start,
                    syscall,
                    args_unfin,
                )),
                |((pid, time), call, (args, omitted))| -> Result<ParsedLine, &str> {
                    Ok(ParsedLine::Opaque(Line {
                        from_pid: pid,
                        time,
                        syscall: String::from(call),
                        argstr: full_argstr(args, omitted),
                        is_opaque: true,
                        duration: None,
                        ret: None,
                    }))
                }
            ),
            map_res(
                tuple((
                    line_start,
                    syscall,
                    args,
                    exit_code
                )),
                |((pid, time), call, (args, omitted), (ret, duration))| -> Result<ParsedLine, &str> {
                    Ok(ParsedLine::Full(Line {
                        from_pid: pid,
                        time,
                        syscall: String::from(call),
                        argstr: full_argstr(args, omitted),
                        is_opaque: false,
                        duration,
                        ret,
                    }))
                }
            )
        )))
    )(i)
}

fn transform_lines(l: Vec<ParsedLine>) -> Option<Vec<Line>> {
    let mut starting = vec![];
    let mut resume = vec![];

    for line in l {
        match line {
            ParsedLine::Resume(v) => resume.push(v),
            ParsedLine::Full(v) => starting.push(v),
            ParsedLine::Opaque(v) => starting.push(v),
        }
    }

    let mut op = OpaqueOutput {
        started: starting,
        resumed: resume,
    };

    op.join()
}

pub fn parse(i: &str) -> IResult<&str, Process> {
    map_res(
        tuple((
            map_res(
                nom::multi::separated_list0(tag("\n"), strace_line),
                |l| -> Result<Vec<Line>, &str> { Ok(transform_lines(l).unwrap()) },
            ),
            opt(tuple((
                alt((
                    tuple((
                        tag("\n"),
                        map_res(match_time, |_| -> Result<&str, &str> { Ok("") }),
                        tag("+++ exited with "),
                    )),
                    tuple((tag(""), tag(""), tag("\n+++ exited with "))),
                )),
                complete::digit1,
                tag(" +++"),
            ))),
        )),
        |(syscalls, ex)| -> Result<Process, std::num::ParseIntError> {
            let mut ordered = syscalls
                .into_iter()
                .map(|x| (x.from_pid, x))
                .into_group_map();
            let root = ordered.get(&None).unwrap().clone();
            let r = root.clone();
            let children = ordered
                .drain()
                .filter(|(k, _)| !k.is_none())
                .map(|x| Process {
                    pid: x.0,
                    syscalls: x.1,
                    children: vec![],
                    exit: None,
                })
                .collect::<Vec<_>>();
            match ex {
                None => Ok(Process {
                    syscalls: r,
                    exit: None,
                    pid: None,
                    children,
                }),
                Some((_, ex, _)) => u8::from_str_radix(ex, 10).map(|exit| Process {
                    syscalls: r,
                    exit: Some(exit),
                    pid: None,
                    children,
                }),
            }
        },
    )(i)
}

#[cfg(test)]
mod tests {
    use super::*;

    impl Return {
        pub fn simple(n: i64) -> Return {
            Return {
                repr: NumRepr { base: 10, num: n },
                r#const: None,
                phrase: None,
            }
        }
    }

    fn extract(l: ParsedLine) -> Line {
        match l {
            ParsedLine::Full(x) => x,
            _ => panic!("invalid data!"),
        }
    }

    #[test]
    fn test_logline() {
        let line = "openat(AT_FDCWD, \"/nix/store/s7l9iwyzkny13sc88appsjf4xrzc5kaa-attr-2.4.48/lib/libattr.so.1\", O_RDONLY|O_CLOEXEC) = 3";
        let res = extract(strace_line(line).unwrap().1);
        assert_eq!("openat", res.syscall);
        assert_eq!(3, res.ret.unwrap().repr.num);
        assert_eq!(3, res.argstr.len());
    }

    #[test]
    fn test_line_with_return_expl() {
        let line = "prctl(PR_SET_KEEPCAPS, 140732031166519) = -1 EINVAL (Invalid argument)";
        let res = extract(strace_line(line).unwrap().1);
        assert_eq!("prctl", res.syscall);
        let u = res.ret.unwrap();
        assert_eq!(-1, u.repr.num);
        assert_eq!("-1", u.repr.code());
        assert_eq!("EINVAL", u.r#const.unwrap());
    }

    #[test]
    fn test_hex_return() {
        let line =
            "mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f256d477000";
        let res = extract(strace_line(line).unwrap().1);
        assert_eq!("mmap", res.syscall);
        assert_eq!("0x7F256D477000", res.ret.unwrap().repr.code());
        assert_eq!(SyscallArg::NULL, *res.argstr.first().unwrap());
        match res.argstr.get(2).unwrap() {
            SyscallArg::Flag(x) => {
                assert_eq!(2, x.flags.len());
            }
            _ => panic!("Expected arg2 to be a flag!"),
        }
    }

    #[test]
    fn test_no_neg_pointer() {
        let line = "mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = -0x7f256d477000";
        assert!(!strace_line(line).is_ok());
    }

    #[test]
    fn test_const_no_pointer() {
        let line = "mmap(NULL, 8192, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x7f256d477000 EINVAL (Invalid argument)";
        assert!(!strace_line(line).is_ok());
    }

    #[test]
    fn test_no_return() {
        let line = "close(3)";
        let res = extract(strace_line(line).unwrap().1);
        assert_eq!("close", res.syscall);
    }

    #[test]
    fn test_omitted_vars() {
        let line = "execve(\"/nix/store/vr96j3cxj75xsczl8pzrgsv1k57hcxyp-coreutils-8.31/bin/echo\", [\"echo\", \"hello\"], 0x7ffd09581bb8 /* 135 vars */) = 0";
        let res = strace_line(line);
        assert!(res.is_ok());
        let r = extract(res.unwrap().1);
        match *r.argstr.last().unwrap() {
            SyscallArg::Omitted(n) => assert_eq!(135, n),
            _ => panic!("Expected last arg to be `Omitted`!"),
        }
    }

    #[test]
    fn test_omitted_vars_bitflags() {
        let line = "execve(\"/nix/store/vr96j3cxj75xsczl8pzrgsv1k57hcxyp-coreutils-8.31/bin/echo\", [\"echo\", \"hello\"], MAP_PRIVATE|MAP_ANONYMOUS /* 135 vars */) = 0";
        let res = strace_line(line);
        assert!(res.is_ok());
        let r = extract(res.unwrap().1);
        let argstr = r.argstr;
        match argstr.last().unwrap() {
            SyscallArg::Omitted(n) => assert_eq!(135, *n),
            _ => panic!("Expected last arg to be `Omitted`!"),
        }

        match argstr.get(1).unwrap() {
            SyscallArg::Array(x) => {
                assert_eq!(2, x.len());
                match x.get(0).unwrap() {
                    SyscallArg::Literal(x) => {
                        assert_eq!("\"echo\"", x);
                    }
                    _ => panic!("Expected list of args!"),
                }
            }
            _ => panic!("Expected second arg to be a list of two!"),
        }
    }

    #[test]
    fn test_omitted_vars_2() {
        let line = "execve(\"/nix/store/vr96j3cxj75xsczl8pzrgsv1k57hcxyp-coreutils-8.31/bin/echo\", [\"echo\", \"hello\"], \"foo\" /* 135 vars */, a) = 0";
        let res = strace_line(line);
        let r = extract(res.unwrap().1);

        // FIXME actually we need an error here: but first we have to properly parse quotes
        match *r.argstr.last().unwrap() {
            SyscallArg::Omitted(_) => panic!("no omitted at the end!"),
            _ => (),
        }
    }

    #[test]
    fn test_pointer_arg() {
        let line = "mprotect(0x7efecfed2000, 16384, PROT_READ) = 0";
        let res = extract(strace_line(line).unwrap().1);
        assert_eq!(3, res.argstr.len());
        match res.argstr.first().unwrap() {
            SyscallArg::Pointer(n) => assert_eq!(139632875216896, n.num),
            _ => panic!("Expected pointer as first arg!"),
        }
    }

    #[test]
    fn test_struct() {
        let line = "fstat(3, {st_mode=S_IFREG|0555, st_size=45104, st_justfortest=NULL, ...}) = 0";
        let result = strace_line(line);
        assert!(result.is_ok());
        let unpack = extract(result.unwrap().1);
        let second = unpack.argstr.get(1).unwrap();
        match second {
            SyscallArg::Struct(x) => {
                assert_eq!(
                    SyscallArg::Literal("45104".to_string()),
                    *(x.get("st_size").unwrap())
                );
                assert_eq!(SyscallArg::NULL, *(x.get("st_justfortest").unwrap()));

                let flag = x.get("st_mode").unwrap();

                match flag {
                    SyscallArg::Flag(x) => {
                        assert_eq!(2, x.flags.len());
                    }
                    _ => panic!("unexpected non-Flag as first arg!"),
                }
            }
            _ => panic!("unexpected second arg!"),
        }
    }

    #[test]
    fn test_long_ret() {
        let line = "set_tid_address(0x7ff1eb116610)         = 20442";
        let result = strace_line(line);
        assert!(result.is_ok());
    }

    #[test]
    fn test_libc_call() {
        let line = "fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x3), ...}) = 0";
        let result = strace_line(line);
        assert!(result.is_ok());

        let second = extract(result.unwrap().1).argstr;
        match second.get(1).unwrap() {
            SyscallArg::Struct(x) => {
                let st_mode = x.get("st_mode").unwrap();
                let st_rdev = x.get("st_rdev").unwrap();

                match st_mode {
                    SyscallArg::Flag(m) => {
                        assert_eq!(2, m.flags.len());
                    }
                    _ => panic!("unexpected val for st_mode!"),
                }
                match st_rdev {
                    SyscallArg::LibcCall(x) => {
                        assert_eq!("makedev", x.name);
                        assert_eq!(2, x.args.len());
                    }
                    _ => panic!("unexpected val for st_rdev!"),
                }
            }
            _ => panic!("unexpected second arg!"),
        }
    }

    #[test]
    fn test_octal_nums() {
        let line = "fchmodat(AT_FDCWD, \"alarm\", 0700)       = 0";
        let result = strace_line(line);

        assert!(result.is_ok());

        let args = extract(result.unwrap().1).argstr;
        match args.get(2).unwrap() {
            SyscallArg::ArbitraryNum(r) => {
                assert_eq!(8, r.base);
                assert_eq!("0o700", r.code());
            }
            _ => panic!("unexpected num!"),
        }
    }

    #[test]
    fn test_exit_group() {
        let line = "exit_group(0)                           = ?";
        let result = strace_line(line);

        assert!(result.is_ok());
        assert_eq!(
            "unknown",
            extract(result.unwrap().1).ret.unwrap().repr.code()
        );
    }

    #[test]
    fn test_process() {
        let lines = "exit_group(0)                           = ?\n+++ exited with 0 +++";
        let result = parse(lines);

        assert!(result.is_ok());
        let proc = result.unwrap().1;

        assert_eq!(Some(0), proc.exit);
    }

    #[test]
    fn test_time() {
        let line = "16:16:43 fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x1), ...}) = 0";
        let result = strace_line(line);

        assert!(result.is_ok());

        let proc = extract(result.unwrap().1);
        let time = proc.time.unwrap();

        assert_eq!(16, time.hour);
        assert_eq!(16, time.minute);
        assert_eq!(43, time.second);
    }

    #[test]
    fn test_time_full_out() {
        let lines = "16:30:31 exit_group(0)                  = ?\n16:30:31 +++ exited with 0 +++";
        let result = parse(lines);

        assert!(result.is_ok());

        assert_eq!(0, result.unwrap().1.exit.unwrap());
    }

    #[test]
    fn test_parse_duration() {
        let line =
            "fstat(1, {st_mode=S_IFCHR|0620, st_rdev=makedev(0x88, 0x1), ...}) = 0 <0.000019>";
        let result = strace_line(line);

        assert!(result.is_ok());
        assert_eq!(0.000019, extract(result.unwrap().1).duration.unwrap());
    }

    #[test]
    fn test_line_with_pid() {
        let line =
            "[pid 31570] sigaltstack({ss_sp=NULL, ss_flags=SS_DISABLE, ss_size=8192}, NULL) = 0";

        let result = strace_line(line);

        assert!(result.is_ok());
    }

    #[test]
    fn test_multi_proc_trivial() {
        let lines = "set_tid_address(0x7ff1eb116610)         = 20442\n[pid 2323] write()";
        let result = parse(lines);

        assert!(result.is_ok());
        let root = result.unwrap();
        let proc = root.1;
        assert_eq!(1, proc.syscalls.len());
        assert_eq!(1, proc.children.len());
        assert_eq!(2323, proc.children.get(0).unwrap().pid.unwrap());

        assert_eq!("set_tid_address", proc.syscalls.get(0).unwrap().syscall);

        assert_eq!(
            "write",
            proc.children
                .get(0)
                .unwrap()
                .syscalls
                .get(0)
                .unwrap()
                .syscall
        );
    }

    #[test]
    fn test_opaque_struct() {
        let mut opaque = OpaqueOutput {
            started: vec![
                (Line {
                    from_pid: None,
                    time: None,
                    syscall: String::from("write"),
                    argstr: vec![],
                    is_opaque: true,
                    ret: None,
                    duration: None,
                }),
            ],
            resumed: vec![(String::from("write"), None, Some(Return::simple(23)))],
        };

        let rs = opaque.join().unwrap();
        let first = rs.get(0).unwrap();
        assert_eq!(23, first.ret.clone().unwrap().repr.num);
        assert!(!first.is_opaque);
    }

    #[test]
    fn test_opaque_with_errors() {
        let l = Line {
            from_pid: None,
            time: None,
            syscall: String::from("write"),
            argstr: vec![],
            is_opaque: true,
            ret: None,
            duration: None,
        };
        let mut opaque1 = OpaqueOutput {
            started: vec![l.clone()],
            resumed: vec![],
        };

        assert!(opaque1.join().is_none());

        let mut opaque2 = OpaqueOutput {
            started: vec![l.clone()],
            resumed: vec![(String::from("foo"), None, Some(Return::simple(23)))],
        };

        assert!(opaque2.join().is_none());
    }

    #[test]
    fn test_clone() {
        let lines = "clone(child_stack=0x7f38) = 8617\n[pid 2323] set_robust(0x7f, 24 <unfinished ...>\n[pid 2323] <... set_robust resumed>) = 0";
        let result = parse(lines);

        assert!(result.is_ok());

        let data = result.unwrap().1;
        assert_eq!(1, data.children.len());
        assert_eq!("clone", data.syscalls.get(0).unwrap().syscall);
        assert_eq!(
            "set_robust",
            data.children
                .get(0)
                .unwrap()
                .syscalls
                .get(0)
                .unwrap()
                .syscall
        );
    }

    #[test]
    fn test_clone_resume_args() {
        let lines = "clone(child_stack=0x7f38) = 8617\n[pid 2323] set_robust(0x7f, 24 <unfinished ...>\n[pid 2323] <... set_robust resumed>NULL) = 0";
        let result = parse(lines);

        assert!(result.is_ok());
        assert!(matches!(
            result
                .unwrap()
                .1
                .children
                .get(0)
                .unwrap()
                .syscalls
                .last()
                .unwrap()
                .argstr
                .last()
                .unwrap(),
            SyscallArg::NULL
        ));
    }
}
